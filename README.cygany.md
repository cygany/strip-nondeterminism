This is the strip-nondeterminism package from the salsa.debian.org, with our minor modifications
to become compilable under cygany, which is a debian overlay over cygwin.

Our cygany branch tracks "master" in the salsa repository, roughly so:

Git remotes:
```
origin   git@gitlab.com:cygany/strip-nondeterminism.git (fetch)
origin   git@gitlab.com:cygany/strip-nondeterminism.git (push)
upstream https://salsa.master.org/reproducible-builds/strip-nondeterminism.git (fetch)
upstream https://salsa.master.org/reproducible-builds/strip-nondeterminism.git (push)
```

Branch setup:
```
* cygany c0deebe [origin/cygany] ...
  master fce2ea3 [upstream/master] ...
```

In our current state, it can be only compiled roughly so:

```
PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules build binary
```

But soon it will be hopefully better.
